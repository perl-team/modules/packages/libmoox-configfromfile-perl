libmoox-configfromfile-perl (0.009-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 11 Dec 2022 00:40:15 +0000

libmoox-configfromfile-perl (0.009-2) unstable; urgency=medium

  * Team upload.
  * Fix autopkgtest.
    Add debian/tests/pkg-perl/smoke-files: the tests also need
    the etc/ directory.
  * Enable more of autopkgtest's syntax.t.
    Add debian/tests/pkg-perl/syntax-skip with one module to skip because it
    needs a package in Suggests, thereby enabling testing of the other
    modules.

 -- gregor herrmann <gregoa@debian.org>  Thu, 21 Feb 2019 18:14:51 +0100

libmoox-configfromfile-perl (0.009-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org.

  [ gregor herrmann ]
  * Update URLs from {search,www}.cpan.org to MetaCPAN.
  * Update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * Simplify rules.
    Stop build-depend on devscripts cdbs.
  * Stop build-depend on dh-buildinfo.
  * Enable autopkgtest.
  * Set Rules-Requires-Root: no.
  * Declare compliance with Debian Policy 4.3.0.
  * Mark build-dependencies needed only for testsuite as such.
  * Relax to (build-)depend unversioned on libmoo-perl:
    Needed version satisfied even in oldstable.
  * Wrap and sort control file.
  * Update copyright info:
    + Strip superfluous copyright signs.
    + Extend coverage of packaging.
    + Use https protocol in Format and Upstream-Contact URLs.
    + Extend coverage for main upstream author.
  * Drop obsolete lintian override regarding debhelper 9.
  * Tighten lintian overrides regarding License-Reference.
  * Update git-buildpackage config: Filter any .git* file.
  * Update watch file:
    + Bump to file format 4.
    + Track only MetaCPAN URL.
    + Rewrite usage comment.
    + Use substitution strings.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 Feb 2019 16:37:24 +0100

libmoox-configfromfile-perl (0.007-1) unstable; urgency=medium

  [ upstream ]
  * Ne release.
    + Separate loading and merging of configfile content.
    + Ensure order of multiple loaded config files.
    + Add support for merging deep structures in config files.
    + Eval all test-libraries to be aware of mistakes.

  [ Jonas Smedegaard ]
  * Modernize git-buildpackage config: Avoid git- prefix.
  * Bump debhelper compatibility level to 9.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage of packaging to include current year.
  * Override lintian regarding license in License-Reference field.
    See bug#786450.
  * Override lintian regarding build-depending unversioned on debhelper.
  * Update package relations:
    + Build-depens on and suggest libhash-merge-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 05 Aug 2015 13:45:21 +0200

libmoox-configfromfile-perl (0.006-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Add rough support for multiple prefixes.
    + Fix incorrect meta-data and incomplete handling of newer eumm
      features.
    + Improve test coverage.
    + Simplify some code to avoid useless conditions.
    + Add new tests for MooX::Cmd and MooX::Options integration.
    + Introduce import setting for config_identifier (consumed attribute
      from MooX::File::ConfigDir).
    + Bump copyright year to 2015.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Extend coverage for main upstream author.
  * Update package relations:
    + Stop needlessly build-depend on libmodule-build-perl: Satisfied by
      perl(-modules) even in oldstable.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Apr 2015 17:41:07 +0200

libmoox-configfromfile-perl (0.005-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Fix links in documentation.
    + Switch to ExtUtils::MakeMaker: Better maintained these days.
    + Add ability for options to importer of MooX::ConfigFromFile.
    + Add option to have a singleton config (loaded once).
    + Improve documentation.
    + Harmonize tests.
    + Update Makefile.PL for up-to-date resources and requirements spec.
    + Consolidate Synopsis in main pod.
    + Move from README to README.md.
    + Improve author tests before releasing.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend.

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 3.9.6.
  * Update copyright info:
    + Extend coverage for main upstream author.
  * Include CDBS perl-makemaker.mk snippet (not perl-build.mk).

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 15 Feb 2015 17:00:17 +0100

libmoox-configfromfile-perl (0.002-2) unstable; urgency=medium

  * Fix build-depend explicitly on libmodule-build-perl.
  * Update watch file to use www.cpan.org URL (in addition to
    metacpan.org URL).

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Jul 2014 18:11:15 +0200

libmoox-configfromfile-perl (0.002-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#748581.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 18 May 2014 21:10:30 +0200
